package com.example.macintosh.quizappproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private RadioButton radioButton1;
    private RadioButton radioButton2;

    private RadioGroup radioGroup2;
    private RadioButton rg2_radioButton1;
    private RadioButton rg2_radioButton2;
    private RadioButton rg2_radioButton3;

    private RadioGroup radioGroup3;
    private RadioButton rg3_radioButton1;
    private RadioButton rg3_radioButton2;

    private CheckBox checkBoxPHP;
    private CheckBox checkBoxJava;
    private CheckBox checkBoxHTML;

    private TextView question1TextView;
    private TextView question2TextView;
    private TextView question3textView;
    private TextView question4textView;
    private TextView question5textView;

    private EditText question1editAnswer;

    private Button submitButton;

    private ArrayList<String> checkboxOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkboxOptions = new ArrayList<>();



        radioGroup = findViewById(R.id.radioGroup);
        radioGroup2 = findViewById(R.id.radioGroup2);
        radioGroup3 = findViewById(R.id.radioGroup3);

        radioButton1 = findViewById(R.id.radioOption1);
        radioButton2 = findViewById(R.id.radioOption2);

        rg2_radioButton1 = findViewById(R.id.rg2radioOption1);
        rg2_radioButton2 = findViewById(R.id.rg2radioOption2);
        rg2_radioButton3 = findViewById(R.id.rg2radioOption3);

        rg3_radioButton1 = findViewById(R.id.rg3radioOption1);
        rg3_radioButton2 = findViewById(R.id.rg3radioOption2);

        checkBoxPHP = findViewById(R.id.phpchkbox);
        checkBoxJava = findViewById(R.id.javachkbox);
        checkBoxHTML = findViewById(R.id.htmlchkbox);

        question1TextView = findViewById(R.id.question1textView);
        question1editAnswer= findViewById(R.id.question1answer);

        question2TextView = findViewById(R.id.question2textView);

        question3textView = findViewById(R.id.question3textView);

        question4textView = findViewById(R.id.question4textView);

        question5textView = findViewById(R.id.question5textView);


        submitButton = findViewById(R.id.submit);

        final Question question1 = new Question();
        question1.setQuestionText("What is the name of the inventor of Java");
        question1.setAnswer("James Gowsling");

        question1TextView.append(question1.displayQuestion());


        final MultipleChoiceQuestion question2 = new MultipleChoiceQuestion();
        question2.setQuestionText("Where was the inventor of Java born?");
        question2.addChoices("Canada",true);
        question2.addChoices("Australia",false);

        question2TextView.append(question2.displayQuestion());
        radioButton1.setText(question2.getChoice(0));
        radioButton2.setText(question2.getChoice(1));


        final MultipleChoiceQuestion question3 = new MultipleChoiceQuestion();
        question3.setQuestionText("Which of these languages are object oriented?");
        question3textView.append(question3.displayQuestion());


        /**This method is invoked to set checkbox listener*/
        setOnCheckBoxStateChangeListener();

        final MultipleChoiceQuestion question4 = new MultipleChoiceQuestion();
        question4.setQuestionText("In Object Oriented Programming, the process of providing a public interface and hiding the implementation details, is called what?");
        question4.addChoices("Polymorphism", false);
        question4.addChoices("Encapsulation",true);
        question4.addChoices("Aggregation",false);

        question4textView.append(question4.displayQuestion());
        rg2_radioButton1.setText(question4.getChoice(0));
        rg2_radioButton2.setText(question4.getChoice(1));
        rg2_radioButton3.setText(question4.getChoice(2));

        final MultipleChoiceQuestion question5 = new MultipleChoiceQuestion();
        question5.setQuestionText("Who is considered the father of Computer Science?");
        question5.addChoices("Tim Berners Lee",false);
        question5.addChoices("Alan Turing",true);

        question5textView.append(question5.displayQuestion());
        rg3_radioButton1.setText(question5.getChoice(0));
        rg3_radioButton2.setText(question5.getChoice(1));


        /*Listener for submit button */
        submitButton.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View view) {

                /**
                 * These are the string values of the checked radio buttons.
                 * */
                if(radioGroup.getCheckedRadioButtonId()==-1 || radioGroup2.getCheckedRadioButtonId()==-1 || radioGroup3.getCheckedRadioButtonId()==-1){
                    Toast.makeText(MainActivity.this, "Please answer all the questions", Toast.LENGTH_SHORT).show();
                }
                else{

                    String radioValueQues2 = ((RadioButton)findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
                    String radioValueQues4 = ((RadioButton)findViewById(radioGroup2.getCheckedRadioButtonId())).getText().toString();
                    String radioValueQues5 = ((RadioButton)findViewById(radioGroup3.getCheckedRadioButtonId())).getText().toString();

                    /**
                     * There two integer variables used to keep track of number of correct and incorrect responses
                     * */
                    int correct= 0;
                    int incorrect =0;


                    boolean question1result = question1.checkAnswer(question1editAnswer.getText().toString());
                    if(question1result) correct++; else incorrect++;

                    boolean question2result = question2.checkAnswer(radioValueQues2);
                    if(question2result) correct++; else incorrect++;

                    boolean question3result = question3.checkAnswerForCheckBoxes(checkboxOptions);
                    if(question3result) correct++; else incorrect++;

                    boolean question4result = question4.checkAnswer(radioValueQues4);
                    if(question4result) correct++; else incorrect++;

                    boolean question5result = question5.checkAnswer(radioValueQues5);
                    if(question5result) correct++; else incorrect++;

                    if(question1.getIsAnsweredState() &&
                            question2.getIsAnsweredState() &&
                            question3.getIsAnsweredState()&&
                            question4.getIsAnsweredState()&&
                            question5.getIsAnsweredState()){


                        callIntent(correct,incorrect);
                    }

                    else Toast.makeText(MainActivity.this, "Please answer all the questions", Toast.LENGTH_SHORT).show();
                }



            }
        });

    }

    public void callIntent(int correct_ans, int incorrect_ans){
        Intent intent = new Intent(this, ScoreActivity.class);
        intent.putExtra("correct",correct_ans);
        intent.putExtra("incorrect",incorrect_ans);
        startActivity(intent);
    }

    /**This method is invoked in the onCreate() of the main_activity
     * sets listeners on each checkbox's state
     * if a checkbox is checked it adds the text of that checkbox to an array
     * if unchecked it removes the text from the array*/
    public void setOnCheckBoxStateChangeListener(){
        checkBoxPHP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) checkboxOptions.add(getString(R.string.php));
                else checkboxOptions.remove(getString(R.string.php));
            }
        });

        checkBoxHTML.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) checkboxOptions.add(getString(R.string.html));
                else checkboxOptions.remove(getString(R.string.html));
            }
        });

        checkBoxJava.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) checkboxOptions.add(getString(R.string.java));
                else checkboxOptions.remove(getString(R.string.java));
            }
        });
    }



}
