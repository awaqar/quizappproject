package com.example.macintosh.quizappproject;

/**
 * Created by macintosh on 13/01/2018.
 */

public class Question {

    private String questionText;
    private String answer;
    private boolean isAnswered;


    public Question() {
        isAnswered = false;
    }

    public void setAnswer(String text){
        this.answer = text;
    }

    public void setQuestionText(String text){
        this.questionText = text;
    }

    public boolean checkAnswer(String response){
        if(response.length()>0) {
            isAnswered = true;
        }
        return response.equalsIgnoreCase(answer);

    }

    public String displayQuestion(){
        return questionText;
    }

    public void setIsAnsweredState(boolean answered){
        isAnswered = answered;
    }

    public boolean getIsAnsweredState(){
        return isAnswered;
    }
}
