package com.example.macintosh.quizappproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ScoreActivity extends AppCompatActivity {


    private TextView correctScoreTextView;
    private TextView incorrectScoreTextView;
    private String correctResponses;
    private String incorrectResponses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        correctScoreTextView = findViewById(R.id.correctscoretextView);
        incorrectScoreTextView = findViewById(R.id.incorrectscoretextView);


        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        correctResponses = extras.get("correct").toString();
        incorrectResponses = extras.get("incorrect").toString();
        correctScoreTextView.append(correctResponses);
        incorrectScoreTextView.append(incorrectResponses);

        Toast.makeText(ScoreActivity.this, "Correct: "+ correctResponses + " Incorrect: " + incorrectResponses, Toast.LENGTH_SHORT).show();

    }

    public void shareScore(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Check out my score in my QuizAppProject I got " + correctResponses + " correct and " + incorrectResponses + " incorrect");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
