package com.example.macintosh.quizappproject;

import java.util.ArrayList;

/**
 * Created by macintosh on 13/01/2018.
 */

public class MultipleChoiceQuestion extends Question {
    private ArrayList<String> choices;

    public MultipleChoiceQuestion(){
        choices = new ArrayList<>();
    }

    /**
     * used for radio buttons
     * adds choices to an array list
     * if a choice is correct/true then it sets that choice
     * as the correct answer to check against.
     * */
    public void addChoices(String choice,boolean correct){
        choices.add(choice);
        if(correct){
            setAnswer(choice);
        }
    }

    /**
     * This method is specifically used for checkBoxes
     * I need more work to improve the logic
     * */
    //TODO: need work on my logic here.

    public boolean checkAnswerForCheckBoxes(ArrayList<String> arrray){
        if(arrray.isEmpty()) return false;

        else{
            setIsAnsweredState(true);

            if(arrray.contains("html")){
                return false;
            }
            else if(arrray.contains("Java") && arrray.contains("php")) return true;

            else return false;
        }

    }

    /**
     * @return value of the choice in the choices array
     * */
    public String getChoice(int i){
        return choices.get(i);
    }
}
